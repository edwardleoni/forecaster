<?php

namespace App\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use App\Helpers\CSVReaderHelper;
use App\Helpers\ForecastHelper;
use Request;

class BasicController extends BaseController
{
    public function showIndex() 
    {
    	return view("index");
    }

    public function showStep2() 
    {
    	$numberOfPositions = Request::input("numberOfPositions");

    	$forecastHelper = new ForecastHelper;
       	$skills = $forecastHelper->findSkillsAvailable();

    	return view("step2", [
    		"numberOfPositions" => $numberOfPositions, 
    		"skills" => $skills
    	]);
    }

    public function processStep2() 
    {
    	$numberOfPositions = Request::input("numberOfPositions");
    	$forecastHelper = new ForecastHelper;
    	$skills = $forecastHelper->findSkillsAvailable();
    	$candidates = [];

    	for ($i = 1; $i <= $numberOfPositions; $i++) {
    		$candidates[$i]["skills"] = Request::input('person_' . $i . '_skills');
    		$candidates[$i]["hours"] = Request::input('person_' . $i . '_hours');
    		$candidates[$i]["duration"] = Request::input('person_' . $i . '_duration');
    		$candidates[$i]["startDate"] = Request::input('person_' . $i . '_startDate');
    		$candidates[$i]["candidates"] = $forecastHelper->findCandidates($candidates[$i]["skills"], $candidates[$i]["hours"], $candidates[$i]["duration"], $candidates[$i]["startDate"]);
    	}

        $teamThatCanStartTheSoonest = false;
    	$attempts = 0;
        while (($attempts < 10) && ($teamThatCanStartTheSoonest === false)) {
            $teamToBe = $forecastHelper->refreshAvailability($candidates);
            $teamThatCanStartTheSoonest = $forecastHelper->findTheSoonestATeamCanStart($teamToBe);
            $attempts++;
        }


    	return view("step2", [
    		"numberOfPositions" => $numberOfPositions, 
    		"skills" => $skills,
    		"candidates" => $candidates,
            'teamThatCanStartTheSoonest' => $teamThatCanStartTheSoonest
    	]);
    }


}
