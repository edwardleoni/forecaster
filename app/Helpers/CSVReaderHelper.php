<?php
namespace App\Helpers;

use CsvReader;

class CSVReaderHelper 
{
  	private $path = null;

  	private $csv = "";

  	public function __construct() 
  	{
  		$this->path = base_path() . "/public/csv/example.csv";
   	}

  	public function getEveryone() 
  	{
		$file = fopen($this->path, "r");
		$i = 0;

		while(($line = fgetcsv($file)) !== FALSE) {
		    if($i == 0) {
		        $c = 0;
		        foreach($line as $col) {
		            $cols[$c] = $col;
		            $c++;
		        }
		    } else if($i > 0) {
		        $c = 0;
		        foreach($line as $col) {
		            $data[$i][$cols[$c]] = $col;
		            $c++;
		        }
		    }
		    $i++;
		}

		$data = $this->sanitizeSkills($data);

		return $data;
  	}

  	public function sanitizeSkills($data) 
  	{
  		for($i=1; $i < count($data); $i++) {
  			$skills = explode(",", $data[$i]["Team Tags"]);
  			$count = 0;

  			foreach ($skills as $each) {
  				$skills[$count] = trim($each);
  				$count++;
  			}

  			$data[$i]["Team Tags"] = $skills;
  		}

  		return $data;
  	}
	

	public static function convertBusinessDaysIntoColumns($amount) 
	{
		// Each normal business week has 5 days, this represents a column in the csv
		return ceil($amount/5);
	}

	


}