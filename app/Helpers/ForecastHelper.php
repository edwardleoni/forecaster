<?php
namespace App\Helpers;

use App\Helpers\CSVReaderHelper;

class ForecastHelper 
{
	private $discardedStartDates = [];

	public function addAvailability(Array $candidates, $hours, $duration) 
	{
		// Finds out how many columns in a row does a candidate have to be available for, after that, the availability can be added
		$columns = CSVReaderHelper::convertBusinessDaysIntoColumns($duration);
		$firstWeekColumn = 3; // Hardcoded position of the first column of the array containing availability data

		for($i=0; $i < count($candidates); $i++) {
			
			$count = 0;
			// Navigates through 
			foreach ($candidates[$i] as $key=>$candidate) {
				$candidates[$i]["availability"] = $this->findAvailability($candidates[$i], $hours, $duration);
			}
		}
		
		return $candidates;
	}

	public function findSkillsAvailable() 
	{
		$csv = new CSVReaderHelper;
		$everyone = $csv->getEveryone();
		$skills = [];

		// Goes through every position
		foreach ($everyone as $eachPerson) {
			$personSkills = $eachPerson["Team Tags"];
			if (!is_array($personSkills)) 
				$personSkills = [$personSkills];
			// Goes through all of each person's skills
			foreach ($personSkills as $personSkill) {
				// Checks if the skills has already been stored
				if (!in_array($personSkill, $skills)) {
					// Another layer of validation for empty strings
					if (!empty($personSkill))
						$skills[] = trim($personSkill);
				}
			}
		}

		return $skills;
	}

	public function findCandidates($skills, $hours, $duration, $startDate)
	{
		$csv = new CSVReaderHelper;
		$candidates = $csv->getEveryone();
		$candidates = $this->filterBySkills($candidates, $skills);
		$candidates = $this->filterByHours($candidates, $hours);
		$candidates = $this->addAvailability($candidates, $hours, $duration);
		$candidates = $this->filterByAvailability($candidates);

		return $candidates;
	}

	public function findAvailability(Array $candidate, $hours, $duration, $startDate=null) 
	{
		$count = 1; // Controller for the loop below
		$availableAt = NULL; 
		$available = 0; // When this matches the availability this means this person is available
		$weeks = CSVReaderHelper::convertBusinessDaysIntoColumns($duration); // Will help check the availability just below

		// Runs through the candidate schedule in search for availability
		foreach ($candidate as $key=>$eachColumn) {
			// Only enters here if hasn't found enough availability
			if ($availableAt === NULL) {
				// 0 = Name, 1 = Skills, 2 = Max hours can work a week, ignore those and start where it matters
				if ($count > $this->checkColumnsToIgnore($candidate, $startDate)) {
					// Also checks if the date isn't a blacklisted
					if (!in_array($key, $this->discardedStartDates)) {
						// If the current week's availability is sufficient
						if (($eachColumn + $hours) <= $candidate["Max Hours/Week"]) {
							$available++;
							$availableAt = ($availableAt == NULL ? $key : NULL);
						} else {
							$available = 0;
							$availableAt = NULL;
						}
					} else {
						//var_dump("second");
						$availableAt = NULL;
						$available = 0;
					}
				}
			}//var_dump($availableAt);

			$count++;
		}//var_dump($availableAt); exit;

		return $availableAt;
	}

	public function refreshAvailability(Array $positionsAndCandidates) 
	{
		for ($i=1; $i <= count($positionsAndCandidates); $i++) { // running through open positions
			for ($j=0; $j < count($positionsAndCandidates[$i]["candidates"]); $j++) { // running through candidates of each position 
				
				$candidate = $positionsAndCandidates[$i]["candidates"][$j];
				$count = 1; // Controller for the loop below
				$availableAt = NULL; 
				$duration = $positionsAndCandidates[$i]["duration"];
				$hours = $positionsAndCandidates[$i]["hours"];
				$startDate = $positionsAndCandidates[$i]["startDate"];
				$available = 0; // When this is set to true, the availability can be set
				$weeks = CSVReaderHelper::convertBusinessDaysIntoColumns($duration); // Will help check the availability just below

				// Runs through the candidate schedule in search for availability
				foreach ($candidate as $key=>$eachColumn) {
					// Only enters here if hasn't found enough availability
					if ($availableAt === NULL) {
						// 0 = Name, 1 = Skills, 2 = Max hours can work a week, ignore those and start where it matters
						if ($count > $this->checkColumnsToIgnore($candidate)) {
							// Also checks if the date isn't a blacklisted
							if (!in_array($key, $this->discardedStartDates)) {								
								// If the current week's availability is sufficient
								if (($eachColumn + $hours) <= $candidate["Max Hours/Week"]) {
									$available++;
									$availableAt = ($availableAt == NULL ? $key : NULL);
								} else {
									$availableAt = NULL;
									$available = 0;
								}
							} else {
								$availableAt = NULL;
								$available = 0;
							}
						}
					}
					$count++;
				}

				$positionsAndCandidates[$i]["candidates"][$j]["availability"] = $availableAt;


			}
		}

		return $positionsAndCandidates;
	}

	public function filterByAvailability(Array $candidates) 
	{
		// Removes candidate if he's not available
		$filteredCandidates = [];
		foreach ($candidates as $candidate) {
			$availability = $candidate["availability"];
			if ($availability != NULL) {
				array_push($filteredCandidates, $candidate);
			}
		}

		return $filteredCandidates;
	}

	public function filterByHours(Array $candidates, $hours) 
	{
		// Removes candidates whose maximum availability is shorter than the required weekly hours
		$filteredCandidates = [];
		foreach ($candidates as $candidate) {
			$hoursAWeek = $candidate["Max Hours/Week"];

			if ($hours <= $hoursAWeek) {
				array_push($filteredCandidates, $candidate);
			}
		}

		return $filteredCandidates;
	}

	public function filterBySkills(Array $candidates, Array $requiredSkills) 
	{
		$filteredCandidates = [];
		foreach ($candidates as $candidate) {
			$candidateSkills = $candidate["Team Tags"];

			// Every team member, or candidate to the position, is a match, until proven otherwise
			$isAMatch = true;
			foreach ($requiredSkills as $eachRequiredSkills) {
				// Just making sure it's an array, for some reason some are slipping through as strings
				if (!is_array($candidateSkills)) {
					$candidateSkills = [$candidateSkills];
				}
	
				if (!in_array($eachRequiredSkills, $candidateSkills)) {
					$isAMatch = false;
				}
			}

			if ($isAMatch === true) {
				array_push($filteredCandidates, $candidate);
			}

		}

		return $filteredCandidates;
	}

	public function findTheSoonestATeamCanStart(Array $positionsAndCandidates) 
	{
		$projectStartDate = $this->findPossibleProjectStartDate($positionsAndCandidates);
		// Checks if there's a possible team with starting on the date
		$team = $this->findIfTheresAPossibleTeam($positionsAndCandidates, $projectStartDate);
		if (!$team) {
			$this->discardedStartDates[] = $projectStartDate;
		}	

		return $team;
	}

	/*
	/	Finds a possible project start date
	/	(which is when the first team member whose start date is 0)
	/   This will later be tested to check if is a good match
	/   Also discards start dates that have been proven not good
	*/
	public function findPossibleProjectStartDate(Array $positionsAndCandidates) 
	{
		// Orders by start date first
		usort($positionsAndCandidates, function($a, $b) {
		    return $a['startDate'] <=> $b['startDate'];
		});

		// Finds out, amongst those with the required skills, the skill that starts first
		$firstPositionToStart = $positionsAndCandidates[0];
		$candidate = $this->findTeamMemberWhoCanStartTheSoonest($firstPositionToStart["candidates"]);

		return $candidate["availability"];
	}		


	public function findTeamMemberWhoCanStartTheSoonest(Array $candidates) 
	{
		// Orders by the first candidate available
		usort($candidates, function($a, $b) {
		    return $a['availability'] <=> $b['availability'];
		});

		if (isset($candidates[0]))
			return $candidates[0];
		else 
			return null;

	}

	/*
	/	Finds the best match (whoever available first), discarding candidates
	/   who have already been tested as not a good match
	*/
	public function findIfTheresAPossibleTeam(Array $positionsAndCandidates, $startDateInDateFormat) 
	{
	
		$team = [];
		$i=1; 
		// Running through positions
		foreach ($positionsAndCandidates as $key=>$each) {
			// Runs through all the candidates and checks if there's one who can fit in
			foreach ($each["candidates"] as $candidate) {
				// Check if there is available for the current schedule
				$available = $this->checkIfCandidateIsAvailableOnSetSchedule($candidate, $each["startDate"], $startDateInDateFormat, $each["duration"], $each["hours"]);
			    
				if ($available !== false) {
					$positionsAndCandidates[$i]["team"] = $available;
					continue;
				}
				
			}

			if (!isset($positionsAndCandidates[$i]["team"])){
				return false;
			}

			$i++;

			
		}

		return $positionsAndCandidates;
	}

	public function checkIfCandidateIsAvailableOnSetSchedule($candidate, $startDate, $startDateInDateFormat, $duration, $hours) 
	{
		$csv = new CSVReaderHelper;
		$columnsToIgnore = $this->checkColumnsToIgnore($candidate, $startDateInDateFormat) + $csv->convertBusinessDaysIntoColumns($startDate);
		$duration = $csv->convertBusinessDaysIntoColumns($duration); 

		$i = 0;
		$j = 0;
		$available = 0;
		foreach ($candidate as $key=>$eachColumn) {
			if (($i >= $columnsToIgnore) && ($i < ($columnsToIgnore + $duration))) { 
				
				if (($eachColumn + $hours) <= $candidate["Max Hours/Week"]) {
					$j++;
					if ($j == 1) {
						$startDateOnTheProject = $key;
					}
				} else {
					$j=0;
				}

				if ($j >= $duration) {
					$available = 1;
					$candidate["startDateOnTheProject"] = $startDateOnTheProject;
					$candidate["startDate"] = $startDate;
					return $candidate; 
				}
				
			}

			$i++;
		}
		return false;

	}

	public function checkColumnsToIgnore(Array $candidate, $startDateInDateFormat=null) 
	{
		// Some of the columns of the csv can be ignored
		// 0 = name, 1 = skills, 2 = max hours a week, hence the 2 below
		if ($startDateInDateFormat === null) {
			return 2;
		} else {
			$count = 0;
			foreach ($candidate as $key=>$eachColumn) {
				if ($startDateInDateFormat == $key) {
					return $count;
				}
				$count ++;
			}
		}
	}

}