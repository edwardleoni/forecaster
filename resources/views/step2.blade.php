@extends('templates.main')
@section('content')
<form action="/step2" method="post">

	@for ($i=1; $i<=$numberOfPositions; $i++)
		<div class="col-md-6">
			<h3>Position {{ $i }}</h3>

			<div class="col-md-6">
				<p><strong>Skills</strong></p>
				<select multiple name="person_{{$i}}_skills[]" style="height:200px; width: 100%" required>
				  	@foreach ($skills as $skill) 
						<option value="{{ $skill }}"  @if (isset($candidates)) {{ (in_array($skill, $candidates[$i]['skills']) ? "selected" : "") }} @endif>{{ $skill }}</option>
				  	@endforeach
				</select>
			</div>
			<div class="col-md-6">
				<p><strong>How many business days do you need this person for?</strong></p>
				<input type="text" name="person_{{$i}}_duration" placeholder="10" value="{{ (isset($candidates[$i]['duration']) ? $candidates[$i]['duration'] : '') }}" required/>
			</div>

			<div class="col-md-6" style="margin-top: 1em;">
				<p><strong>How many hours a week will this person be working on this project?</strong></p>
				<input type="text" name="person_{{$i}}_hours" placeholder="20" required value="{{ (isset($candidates[$i]['hours']) ? $candidates[$i]['hours'] : '') }}"/>
			</div>

			<div class="col-md-6" style="margin-top: 1em;">
				<p><strong>When do you need this person to start, after the project start date?</strong></p>
				<input type="text" name="person_{{$i}}_startDate" placeholder="0 for asap" required value="{{ (isset($candidates[$i]['startDate']) ? $candidates[$i]['startDate'] : '') }}"/>
			</div>
		</div>

		<div class="col-md-6" style="padding: 3em 0">
			@if (isset($candidates))
			<div class="col-md-12" >
				@if (count($candidates[$i]["candidates"]) > 0)
					<h4>Matching Candidates</h4>
					<ul>
					@foreach ($candidates[$i]["candidates"] as $candidate)
						<li>{{ $candidate["Person"] }} - Available at <i>{{ $candidate["availability"] }}</i></li>
					@endforeach
					</ul>
				@else
					<p>No candidates with the required skills were found</p>
				@endif
			</div>
			@endif
		</div>

		<div style="clear:both"></div>


	@endfor

	@if (isset($teamThatCanStartTheSoonest))
		<div class="col-md-12">
			<h3>The team that is available the soonest</h3>
			<table class="col-md-12">
				<tr>
					<th>Position</th>
					<th>Position Skills</th>
					<th>Candidate</th>
					<th>Candidate Skills</th>
					<th>Start Date on the Project</th>
				</tr>

				@for ($i=1; $i <= count($teamThatCanStartTheSoonest); $i++)
					<tr>
						<td>Position {{ $i }}</td>
						<td>
						@if (isset($teamThatCanStartTheSoonest[$i]["skills"]))
							@foreach ($teamThatCanStartTheSoonest[$i]["skills"] as $tag)
							    {{ $tag }}
							@endforeach
						@else
						@endif
						</td>

						
						@if (isset($teamThatCanStartTheSoonest[$i]["team"]))
							<td>{{ $teamThatCanStartTheSoonest[$i]["team"]["Person"] }}</td>
							<td>
							@if (is_array($teamThatCanStartTheSoonest[$i]["team"]["Team Tags"])) 
								@foreach ($teamThatCanStartTheSoonest[$i]["team"]["Team Tags"] as $tag)
							    	{{ $tag }}
								@endforeach
							@else
								{{ $teamThatCanStartTheSoonest[$i]["team"]["Team Tags"] }}
							@endif
							
							</td>
							<td> The week of {{ $teamThatCanStartTheSoonest[$i]["team"]["startDateOnTheProject"] }}</td>
						@else
							<td></td>
							<td></td>
							<td></td>
						@endif
						
				@endfor
			</table>

		</div>

	@endif
	
	<input type="hidden" value="{{ $numberOfPositions }}" name="numberOfPositions"/>
	<div class="col-md-12">
		<input type="submit" value="Find" style="float: right; width: 200px; position: relative; margin-bottom: 1em;"/>
	</div>
	
</form>
@stop