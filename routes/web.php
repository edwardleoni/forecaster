<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', "BasicController@showIndex");
Route::get('/step2', "BasicController@showStep2");
Route::post('/step2', "BasicController@processStep2");
